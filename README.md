# EEE3088F Project repo

Project with the purpose of a micro-Hat design for a Pi-Zero

# Project description

Our Micro-Hat acts as a UPS for  the PI-zero and performs the following tasks:

	1. Takes a voltage from mains which is higher than 5V. (15V in our case)
	2. Converts this voltage into two values. One out of the linear voltage regulator for 
	   the battery charger circuit. The second goes to the SMPS IC and is converted to 5V.
	3. If there is a power failure. The batteries that have been charging take over and supply power.
	4. The batteries will provide 2.5 A for 1 hour before reaching full discharge.
	5. After full discharge, the micro-hat can no longer supply power and the Pi-zero will shutdown.

# BILL OF MATERIALS

# Power circuit
x1 LT317 
x2 LT1074
x2 1000uF capacitors
x1 47nF capacitor
x1 220uF capacitor
x1 0.01uF capacitor
x1 50uH inductor
x4 RBR3MM40A diodes
x1 MBR745 diode
x2 BC547B NPN transistors
x1 11.2V nominal voltage 2.5 Ah lithium-ion cell
x2 1K ohm resistors
x1 0.8 ohm resistor
x1 2k7 ohm resistor
x1 1k9 ohm resistor

# op-amp circuit
x1 LT1413 op-amp
x2 10k ohm resistors
x1 20k ohm resistor
x1 8k2 ohm resistor
x1 3.3 V zener

# LED circuit
x3 green LED's
x2 1k ohm resitors
x1 2k2 ohm resistors

To be updated with exact values and prices

# How to use
Download content. All .asc files must be run on LTSpice. Currently there are no KI-cad files added. Will be added
at a later date. To add more items, commit items to be added and they will be reviewed.
